<?php

// this script assumes Drupal 10, and one database for both CiviCRM and Drupal

# DEFINE PATHS
$host = exec("cd && realpath .", $output, $retval);
$host = $output[0];
$tmp = "$host/tmp";
$path_to_httpdocs = "$host/httpdocs";
$destination_dir = "$host/httpdocs/dev";
$destination_public_dir = "$host/httpdocs/dev/web";
$destination_civicrm_settings_php = "$host/httpdocs/dev/web/sites/default/civicrm.settings.php";
$destination_settings_php = "$host/httpdocs/dev/web/sites/default/settings.php";
$source_public_dir = "$host/httpdocs/pro/web";

# GET DATABASE NAMES, USER NAMES AND BASE URLS FROM SETTINGS FILES
$handle_for_settings_dev = fopen("$host/httpdocs/dev/web/sites/default/settings.php", 'r');
$handle_for_settings_pro = fopen("$host/httpdocs/pro/web/sites/default/settings.php", 'r');
$file_array_dev = file("$host/httpdocs/dev/web/sites/default/settings.php"); // file() reads an entire file into an array.
$file_array_pro = file("$host/httpdocs/pro/web/sites/default/settings.php");
$file_array_dev_civicrm = file("$host/httpdocs/dev/web/sites/default/civicrm.settings.php");
$file_array_pro_civicrm = file("$host/httpdocs/pro/web/sites/default/civicrm.settings.php");
if($file_array_dev == false or $file_array_pro == false or $file_array_dev_civicrm == false or $file_array_pro_civicrm == false) {
    checkError(1, "Could not convert file to array", NULL);
}
else {
    // DEV SYSTEM: infos from civicrm.settings.php
    $destination_URL = "";
    $data_dev_civicrm = array();
    foreach ($file_array_dev_civicrm as $key => $line) {
        if(str_contains($line, "define( 'CIVICRM_UF_BASEURL'") and !str_contains($line, "*")) {
            // the string define( 'CIVICRM_UF_BASEURL' is not a comment
            $destination_URL = trim(explode(",", $line)[1], " ;)'\t\r\n");
            $destination_URL = explode("//", $destination_URL)[1];
        }
    }
    // DEV SYSTEM: infos from settings.php
    $data_dev = array();
    foreach ($file_array_dev as $key => $line) {
        if(str_contains($line, '$databases[\'default\'][\'default\']') and !str_contains($line, "*")) {
            // the string $databases['default']['default'] is not a comment
            $counter = 1;
            $next_line = $file_array_dev[$key+$counter];
            while(!str_contains($next_line, ");")) {
                // the next line is not yet the end of the database array
                $data_dev[] = $next_line;  // add this line to the $data that we want
                $counter = $counter+1;
                $next_line = $file_array_dev[$key+$counter];
            }
        }
    }
    $destination_DB = explode(" ", trim($data_dev[0], " ,\n\r\t"))[2];
    $substringsToRemove = ['\'', '"'];
    $destination_DB = str_replace($substringsToRemove, "", $destination_DB);
    $username_dev = explode(" ", trim($data_dev[1], " ,\n\r\t"))[2];
    $username_dev = str_replace($substringsToRemove, "", $username_dev);
    $pw_dev = explode(" ", trim($data_dev[2], " ,\n\r\t"))[2];
    $pw_dev = str_replace($substringsToRemove, "", $pw_dev);
    
    // PRO SYSTEM: infos from civicrm.settings.php
    $source_URL = "";
    $data_pro_civicrm = array();
    foreach ($file_array_pro_civicrm as $key => $line) {
        if(str_contains($line, "define( 'CIVICRM_UF_BASEURL'") and !str_contains($line, "*")) {
            // the string define( 'CIVICRM_UF_BASEURL' is not a comment
            $source_URL = trim(explode(",", $line)[1], " ;)'\t\r\n");
            $source_URL = explode("//", $source_URL)[1];
        }
    }
    // PRO SYSTEM: infos from settings.php
    $data_pro = array();
    foreach ($file_array_pro as $key => $line) {
        if(str_contains($line, '$databases[\'default\'][\'default\']') and !str_contains($line, "*")) {
            // the string $databases['default']['default'] is not a comment
            $counter = 1;
            $next_line = $file_array_pro[$key+$counter];
            while(!str_contains($next_line, ");")) {
                // the next line is not yet the end of the database array
                $data_pro[] = $next_line;  // add this line to the $data that we want
                $counter = $counter+1;
                $next_line = $file_array_pro[$key+$counter];
            }
        }
    }
    $source_DB = explode(" ", trim($data_pro[0], " ,\n\r\t"))[2];
    $source_DB = str_replace($substringsToRemove, "", $source_DB);
    $username_pro = explode(" ", trim($data_pro[1], " ,\n\r\t"))[2];
    $username_pro = str_replace($substringsToRemove, "", $username_pro);
    $pw_pro = explode(" ", trim($data_pro[2], " ,\n\r\t"))[2];
    $pw_pro = str_replace($substringsToRemove, "", $pw_pro);
}
fclose($handle_for_settings_dev);
fclose($handle_for_settings_pro);


# CREATE TEMPORARY CONFIG FILES TO STORE LOGIN DATA
## SOURCE SYSTEM login for mysqldump
$source_DB_login = tempnam($tmp, '.mysql');
file_put_contents($source_DB_login, "[mysqldump]
user=$username_pro
password=$pw_pro");
## DESTINATION SYSTEM for mysql
$destination_DB_login_mysql = tempnam($tmp, '.mysql');
file_put_contents($destination_DB_login_mysql, "[mysql]
user=$username_dev
password=$pw_dev");
## DESTINATION SYSTEM for mysqldump
$destination_DB_login_mysqldump = tempnam($tmp, '.mysql');
file_put_contents($destination_DB_login_mysqldump, "[mysqldump]
user=$username_dev
password=$pw_dev");


//set PHP timeout
set_time_limit(240);
// change to tmp directory
exec(" cd $tmp", $output, $retval);
// exec() returns the status $retval (0 if successful) and the output of the current command in $output
$output = null;
$retval = -1;

# BACKUP DATABASE OF DESTINATION SYSTEM
date_default_timezone_set('Europe/Berlin');
$date =  date("Y.m.d.-H:i");
$filename = $destination_DB . "_" . $date . ".sql";
$mysql_command = " mysqldump --defaults-file=$destination_DB_login_mysqldump $destination_DB > $host/backups/archive/$filename";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysqldump $destination_DB dump to backups folder -- Returned with status $retval \n";
checkError($retval, "could not dump $destination_DB to backups folder\n", $output);
unset($output);
$retval = -1;
unlink($destination_DB_login_mysqldump);


# COMPRESS BACKUP OF DATABASE OF DESTINATION SYSTEM
// -m removes the original file and keeps only the compressed one
$mysql_command = " zip -m $host/backups/archive/$filename.gz $host/backups/archive/$filename";
$mysql_command = exec($mysql_command, $output, $retval);
echo "zip the $destination_DB to backups folder -- Returned with status $retval \n";
checkError($retval, "could not zip the $destination_DB to backups folder\n", $output);
unset($output);
$retval = -1;

# BACKUP CODE OF DESTINATION SYSTEM
$zip = exec("zip -r -9 \"$host/backups/archive/complete_dev-$date.zip\" \"$host/httpdocs/dev\"", $output, $retval);
echo "zip the code base to $host/backups/archive/complete_dev-$date.zip -- Returned with status $retval \n";
checkError($retval, "could not zip the code base to $host/backups/archive/complete_dev-$date.zip\n", $output);
unset($output);
$retval = -1;

# GET DUMP OF SOURCE SYSTEM DATABASE
$mysql_command = " mysqldump --defaults-file=$source_DB_login $source_DB > $tmp/$source_DB.sql";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysqldump $source_DB dump to tmp folder -- Returned with status $retval \n";
checkError($retval, "could not dump $source_DB to tmp folder\n", $output);
unset($output);
unlink($source_DB_login);
$retval = -1;

# CHANGE VALUES INSIDE THE DB DUMP
// change name of database
$sed = exec(" sed -i 's/$source_DB/$destination_DB/gi' $tmp/$source_DB.sql", $output, $retval);
echo "sed replaces in $source_DB.sql: $source_DB --> $destination_DB -- Returned with status $retval\n";
checkError($retval, "could not sed $source_DB\n", $output);
unset($output);
$retval = -1;
// change name of URL
$sed = exec(" sed -i 's/$source_URL/$destination_URL/gi' $tmp/$source_DB.sql", $output, $retval);
echo "sed replaces in $source_DB.sql: $source_URL --> $destination_URL -- Returned with status $retval \n";
checkError($retval, "could not sed $source_URL\n", $output);
unset($output);
$retval = -1;

# SMARTGROUPS: REPLACE STRING LENGTH OF URL IN SQL DUMP
$handle_for_reading = fopen("$tmp/$source_DB.sql", 'r');
$handle_for_writing = fopen("$tmp/$source_DB.sql.tmp", 'w');
$searched_string = "INSERT INTO `civicrm_saved_search`";
// calculate length difference between $destination_URL and $source_URL
$length_of_source_URL = strlen($source_URL);
$length_of_destination_URL = strlen($destination_URL);
// take each line of SQL dump and check if it contains the insert statement "INSERT INTO `civicrm_saved_search`"
// if yes, modify the lengths of all URLs inside this line; if not just copy the line
if($handle_for_reading == false or $handle_for_writing == false) {
    echo "Could not open file $tmp/$source_DB.sql or file $tmp/$source_DB.sql.tmp\n";
    die;
}
while (!feof($handle_for_reading)) {
    $line = fgets($handle_for_reading);
    $position = strpos($line, $searched_string); // position in the current line (returns 0 or false, here). The whole SQL statement is in one line, so that's ok.
    if ($position !== false) {
        // Insert statement "INSERT INTO `civicrm_saved_search`" has been found in this line
        $search_URL = "https://" . $destination_URL;
        $URL_position = 0;
        $last_URL_position = 0;
        $new_line = $line;
        $length_of_last_firstpart = 0;
        $counter = 0;
        // search one occurrence of URL at a time, and correct the length
        while (($URL_position = strpos($new_line, $search_URL, $URL_position)) !== false) {
            $counter += 1;
            // $URL_position: position where URL has been found (unrelated to the offset, counting from the start of $line)
            // split the line at the current $URL_position
            $first_part = substr($new_line, 0, $length_of_last_firstpart + $URL_position - $last_URL_position);
            $second_part = substr($new_line, $URL_position);
            // read the length of the complete old URL, just in front of the current instance of URL
            $complete_length_source_URL = explode(":", $first_part);
            $complete_length_source_URL = $complete_length_source_URL[count($complete_length_source_URL) - 2];
            $numlength = strlen((string)$complete_length_source_URL);
            // calculate the new complete URL length of the current instance of URL
            $complete_length_destination_URL = $complete_length_source_URL;
            if ($length_of_destination_URL < $length_of_source_URL) {
                // URL gets shorter
                $length_difference = $length_of_source_URL - $length_of_destination_URL;
                $complete_length_destination_URL = strval($complete_length_source_URL - $length_difference);
            } elseif ($length_of_source_URL < $length_of_destination_URL) {
                // URL gets longer
                $length_difference = $length_of_destination_URL - $length_of_source_URL;
                $complete_length_destination_URL = strval($complete_length_source_URL + $length_difference);
            }
            $first_part = substr_replace($first_part, $complete_length_destination_URL, -($numlength + 3), -3);
            $length_of_last_firstpart = strlen($first_part);
            // put two parts of line together again
            $new_line = $first_part . $second_part;
            $last_URL_position = $URL_position;
            $URL_position = $URL_position + strlen($search_URL); // set $URL_position to the position after the last instance of URL that has been found
        }
        fputs($handle_for_writing, $new_line); // writes line to write file (either modified or not)
        echo "Replaced all $counter URL lengths in the file $source_DB.sql to keep smartgroups functional.\n";
    } else {
        // this line is not the insert statement "INSERT INTO `civicrm_saved_search`" and can just be copied
        fputs($handle_for_writing, $line); // writes line of read file to write file
    }
}
fclose($handle_for_reading);
fclose($handle_for_writing);
// replace the read file with the written file
rename("$tmp/$source_DB.sql.tmp", "$tmp/$source_DB.sql");


# DROP ALL TABLES IN DESTINATION DB (so tables that have possibly been created by extensions will be deleted)
// delete old drop_tables if it exists
$mysql_command = " mysql --defaults-file=$destination_DB_login_mysql -D $destination_DB -e 'DROP TABLE IF EXISTS drop_tables;'";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysql $destination_DB login and SQL command (delete old drop_tables table)-- Returned with status $retval\n";
checkError($retval, "could not log in $destination_DB or apply the SQL command (delete old drop_tables table)\n", $output);
unset($output);
$retval = -1;
// create a table called drop_tables that contains all drop commands
$sql_create_drop_tables_file = "$tmp/create_drop_tables";
file_put_contents($sql_create_drop_tables_file, "
CREATE TABLE drop_tables (ID int PRIMARY KEY AUTO_INCREMENT, statement VARCHAR(500)); 
INSERT INTO drop_tables (statement) SELECT CONCAT('DROP TABLE IF EXISTS `', TABLE_SCHEMA, '`.`', TABLE_NAME, '`;')
FROM information_schema.TABLES
WHERE TABLE_SCHEMA = '$destination_DB'
AND NOT TABLE_NAME='drop_tables'; 
");
$mysql_command = " mysql --defaults-file=$destination_DB_login_mysql $destination_DB < $sql_create_drop_tables_file ";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysql $destination_DB login and SQL command (create the drop_tables table)-- Returned with status $retval\n";
checkError($retval, "could not log in $destination_DB or apply the SQL command (create the drop_tables table)\n", $output);
unset($output);
$retval = -1;
// how many rows exist in drop_tables?
$mysql_command = " mysql --defaults-file=$destination_DB_login_mysql -D $destination_DB -e 'SELECT COUNT(*) FROM drop_tables;'";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysql $destination_DB login and SQL command (count rows of the drop_tables table)-- Returned with status $retval\n";
checkError($retval, "could not log in $destination_DB or apply the SQL command (count rows of the drop_tables table)\n", $output);
$number_of_rows = $output[1];
unset($output);
$retval = -1;
// use a prepared statement to loop through all the drop_tables rows and remove this table
echo "Preparing and executing DROP statements for all $number_of_rows tables in drop_tables\n";
for ($i = 1; $i <= $number_of_rows; $i++) {
    $mysql_command = " mysql --defaults-file=$destination_DB_login_mysql -D $destination_DB -e 'SET FOREIGN_KEY_CHECKS=0; SELECT statement INTO @sql FROM drop_tables WHERE id = $i;
PREPARE sql_query FROM @sql; EXECUTE sql_query;'";
    $mysql_command = exec($mysql_command, $output, $retval);
    // echo "mysql $destination_DBm login and SQL command (prepare and execute DROP statement for table with ID $i)-- Returned with status $retval\n";
    checkError($retval, "could not log in $destination_DB or apply the SQL command (prepare and execute DROP statement for table with ID $i)\n", $output);
    $retval = -1;
}
unset($output);
// delete drop_tables table
$mysql_command = " mysql --defaults-file=$destination_DB_login_mysql -D $destination_DB -e 'DROP TABLE IF EXISTS drop_tables;'";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysql $destination_DB login and SQL command (delete current drop_tables table)-- Returned with status $retval\n";
checkError($retval, "could not log in $destination_DB or apply the SQL command (delete current drop_tables table)\n", $output);
unset($output);
$retval = -1;


# COPY THE DB FROM SOURCE SYSTEM TO DESTINATION SYSTEM
$mysql_command = " mysql --defaults-file=$destination_DB_login_mysql $destination_DB < $tmp/$source_DB.sql";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysql $destination_DB import $source_DB.sql -- Returned with status $retval\n";
checkError($retval, "could not import $source_DB.sql\n", $output);
unset($output);
$retval = -1;


# ANONYMIZE
$sql_anon_command_file = "$tmp/sql_anon_command";
file_put_contents($sql_anon_command_file, "
UPDATE civicrm_contact INNER JOIN civicrm_contact AS c2 ON civicrm_contact.id = c2.id SET civicrm_contact.sort_name = CONCAT( 'Anonymous', ', ', c2.id ) WHERE civicrm_contact.id = c2.id;
  UPDATE civicrm_contact INNER JOIN civicrm_contact AS c2 ON civicrm_contact.id = c2.id SET civicrm_contact.display_name = CONCAT( c2.id, ' ', 'Anonymous' ) WHERE civicrm_contact.id = c2.id;
  UPDATE civicrm_contact INNER JOIN civicrm_contact AS c2 ON civicrm_contact.id = c2.id SET civicrm_contact.last_name = 'Anonymous' WHERE civicrm_contact.id = c2.id;
  UPDATE civicrm_contact INNER JOIN civicrm_contact AS c2 ON civicrm_contact.id = c2.id SET civicrm_contact.first_name = c2.id WHERE civicrm_contact.id = c2.id;
  UPDATE civicrm_contact SET birth_date = STR_TO_DATE('01,1,1999','%d,%m,%Y');
  UPDATE civicrm_address SET street_address = 'Anonymous', supplemental_address_1 = 'Anonymous', postal_code = 'Anonymous';
  UPDATE civicrm_address SET postal_code = null,postal_code_suffix = null,geo_code_1 = null,geo_code_2 = null;
  UPDATE civicrm_contact SET email_greeting_display = 'Dear Anonymous', addressee_display = 'Anonymous , Anonymous';
  UPDATE civicrm_contact SET postal_greeting_display = 'Dear Anonymous', addressee_display = 'Anonymous , Anonymous';
  UPDATE civicrm_email INNER JOIN civicrm_email AS e2 ON civicrm_email.id = e2.id SET civicrm_email.email = CONCAT( e2.id ,'@','example.com' ) WHERE civicrm_email.id = e2.id;
  UPDATE civicrm_phone SET phone = md5(phone);
  UPDATE civicrm_note SET note = md5(note), subject = md5(subject);");
$mysql_command = " mysql --defaults-file=$destination_DB_login_mysql $destination_DB < $sql_anon_command_file ";
$mysql_command = exec($mysql_command, $output, $retval);
echo "mysql $destination_DB login and SQL command (anonymize)-- Returned with status $retval\n";
checkError($retval,  "could not log in $destination_DB or apply the SQL command (anonymize)\n", $output);
unset($output);
$retval = -1;
unlink($destination_DB_login_mysql);


# CLEAN UP SQL COMMANDS FILES
$remove = exec(" rm $sql_create_drop_tables_file $sql_anon_command_file", $output, $retval);
echo "rm SQL commands files -- Returned with status $retval\n";
unset($output);
$retval = -1;

# CLEAN UP THE DUMP FILES
$remove = exec(" rm $tmp/$source_DB.sql", $output, $retval);
unset($output);
$retval = -1;

# set the permissions of default dir (Destination System) to writable
// the 755 permission translates to rwx r-x r-x
$permissions = exec(" chmod -R 755 $destination_public_dir/sites/default", $output, $retval);
echo "change permissions of $destination_public_dir/sites/default to 755 -- Returned with status $retval\n";
unset($output);
$retval = -1;

# set the permissions of settings.php, civicrm.settings.php to read only
// the 400 permission translates to r-- --- ---
$permissions = exec(" chmod 400 $destination_settings_php", $output, $retval);
echo "change permissions of settings.php to 400 -- Returned with status $retval\n";
unset($output);
$retval = -1;
$permissions = exec(" chmod 400 $destination_civicrm_settings_php", $output, $retval);
echo "change permissions of civicrm.settings.php to 400 -- Returned with status $retval\n";
unset($output);
$retval = -1;

# CLEAN UP FILES IN DESTINATION SYSTEM
$remove = exec(" find $destination_public_dir -not -wholename '$destination_civicrm_settings_php' -not -wholename '$destination_settings_php' -delete", $output, $retval);
echo "find & delete content of $destination_public_dir -- Returned with status $retval\n";
unset($output);
$retval = -1;

# COPY FILES FROM SOURCE TO DESTINATION SYSTEM
// keeps permissions of copied files (default dir: r-xr-xr-x)
// rsync only overwrites when there are differences (checked by SIZE and TIME of last change) or based on checksum if -c param is passed
$copy = exec(" rsync -avc --exclude 'civicrm.settings.php' --exclude 'settings.php' $source_public_dir $destination_dir/", $output, $retval);
echo "rsync content from $source_public_dir to $destination_dir/ -- Returned with status $retval\n";
unset($output);
$retval = -1;

# ADD ROBOTS.TXT TO DESTINATION SYSTEM
// /httpdocs/dev/web
$file = $destination_public_dir . "/robots.txt";
$data = "User-agent: *\nDisallow: /";
file_put_contents($file, $data);


# clear cache
$cache = exec(" $host/httpdocs/dev/vendor/drush/drush/drush cr", $output, $retval);
unset($output);
$retval = -1;

echo "Reached end of skript.\n";

function checkError($retval, $errormessage, $output) {
    if($retval != 0) {
        echo "OUTPUT OF FAILED COMMAND:\n\n";
        foreach($output as $output_line)  {
            echo "$output_line\n";
        }
        trigger_error("$errormessage\n", E_USER_ERROR);
    }
}

?>